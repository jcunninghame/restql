package com.tyro.restql.grammar.token;

import java.util.Objects;

public abstract class Token {

    private final String symbol;
    private final int precedence;
    private final int rightPrecedence;
    private final int nextPrecedence;

    public Token(String symbol, int precedence, int rightPrecedence, int nextPrecedence) {
        this.symbol = symbol;
        this.precedence = precedence;
        this.rightPrecedence = rightPrecedence;
        this.nextPrecedence = nextPrecedence;
    }

    public int precedence() {
        return precedence;
    }

    public int rightPrecedence() {
        return rightPrecedence;
    }

    public int nextPrecedence() {
        return nextPrecedence;
    }

    public String getSymbol() {
        return symbol;
    }

    @Override
    public String toString() {
        return getSymbol();
    }

    @Override
    public boolean equals(Object obj) {
        return Objects.equals(this.getClass(), obj.getClass());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.getClass());
    }
}
