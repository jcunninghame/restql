package com.tyro.restql.grammar.tree.builder;

import com.tyro.restql.grammar.token.Token;
import com.tyro.restql.grammar.token.Value;
import com.tyro.restql.grammar.tree.BinaryNode;
import com.tyro.restql.grammar.tree.Node;
import com.tyro.restql.grammar.tree.UnaryNode;
import com.tyro.restql.grammar.tree.ValueNode;

import static com.tyro.restql.grammar.tree.builder.TreeBuilder.LeafBuilder.value;

public class TreeBuilder {

    public static class BinaryTreeBuilder {

        private Token token;

        public BinaryTreeBuilder(Token token) {
            super();
            this.token = token;
        }

        public static BinaryTreeBuilder binary(Token token) {
            return new BinaryTreeBuilder(token);
        }

        public BinaryTreeLeftBuilder left(Node leftChild) {
            return new BinaryTreeLeftBuilder(token, leftChild);
        }

        public BinaryTreeLeftBuilder left(String value) {
            return new BinaryTreeLeftBuilder(token, value(value));
        }

        public static class BinaryTreeLeftBuilder {

            private Token token;
            private Node leftChild;

            private BinaryTreeLeftBuilder(Token token, Node leftChild) {
                super();
                this.token = token;
                this.leftChild = leftChild;
            }

            public BinaryNode right(Node rightChild) {
                return new BinaryNode(token, leftChild, rightChild);
            }

            public BinaryNode right(String value) {
                return new BinaryNode(token, leftChild, value(value));
            }
        }
    }

    public static class UnaryTreeBuilder {

        private Token token;

        public UnaryTreeBuilder(Token token) {
            super();
            this.token = token;
        }

        public static UnaryTreeBuilder unary(Token token) {
            return new UnaryTreeBuilder(token);
        }

        public UnaryNode child(Node child) {
            return new UnaryNode(token, child);
        }

        public Node child(String value) {
            return new UnaryNode(token, value(value));
        }
    }

    public static class LeafBuilder {

        public static Node value(String value) {
            return new ValueNode(new Value(value));
        }
    }
}
