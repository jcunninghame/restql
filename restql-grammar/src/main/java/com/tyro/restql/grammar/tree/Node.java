package com.tyro.restql.grammar.tree;

import com.tyro.restql.grammar.token.Token;
import com.tyro.restql.grammar.visitor.QueryVisitor;

public interface Node {

    Token getToken();

    <T> T accept(QueryVisitor<T> visitor);
}