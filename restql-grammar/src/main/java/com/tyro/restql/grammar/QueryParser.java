package com.tyro.restql.grammar;

import com.tyro.restql.grammar.token.*;
import com.tyro.restql.grammar.tree.BinaryNode;
import com.tyro.restql.grammar.tree.Node;
import com.tyro.restql.grammar.tree.UnaryNode;
import com.tyro.restql.grammar.tree.ValueNode;

public class QueryParser {

    private final TokenizedQuery tokenizedQuery;

    public QueryParser(TokenizedQuery tokenizedQuery) {
        this.tokenizedQuery = tokenizedQuery;
    }

    public Node parse() {
        Node node = evaluate(0, evaluate());
        tokenizedQuery.pop(Terminal.class);
        return node;
    }

    private Node evaluate(int precedence, Node node) {
        while (tokenizedQuery.peekType(Binary.class, Prefix.class, Postfix.class)
                && precedence <= tokenizedQuery.peek().precedence()) {
            if (tokenizedQuery.peekType(Binary.class)) {
                node = handleBinary(node);
            } else if (tokenizedQuery.peekType(Prefix.class)) {
                node = handlePrefix(node);
            } else {
                node = handlePostfix(node);
            }
        }
        return node;
    }

    private Node evaluate() {
        tokenizedQuery.expect(GroupOpen.class, Value.class, Prefix.class);
        if (tokenizedQuery.peekType(GroupOpen.class)) {
            return handleGroup();
        } else if (tokenizedQuery.peekType(Prefix.class)) {
            return handlePrefix();
        } else {
            return handleValue();
        }
    }

    private Node handleBinary(Node leftChildNode) {
        Token binaryToken = tokenizedQuery.pop(Binary.class);
        return new BinaryNode(binaryToken, leftChildNode, evaluate(binaryToken.rightPrecedence(), evaluate()));
    }

    private Node handlePostfix(Node childNode) {
        Token modifierToken = tokenizedQuery.pop(Postfix.class);
        return new UnaryNode(modifierToken, evaluate(modifierToken.rightPrecedence(), childNode));
    }

    private Node handlePrefix(Node childNode) {
        Token modifierToken = tokenizedQuery.pop(Prefix.class);
        return new UnaryNode(modifierToken, evaluate(modifierToken.rightPrecedence(), childNode));
    }

    private Node handlePrefix() {
        Token modifierToken = tokenizedQuery.pop(Prefix.class);
        return new UnaryNode(modifierToken, evaluate());
    }

    private Node handleValue() {
        Token valueToken = tokenizedQuery.pop(Value.class);
        return new ValueNode(valueToken);
    }

    private Node handleGroup() {
        Token groupToken = tokenizedQuery.pop(GroupOpen.class);
        Node node = evaluate(groupToken.rightPrecedence(), evaluate());
        tokenizedQuery.pop(GroupClose.class);
        return node;
    }
}
