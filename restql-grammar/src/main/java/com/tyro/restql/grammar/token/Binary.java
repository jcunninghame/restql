package com.tyro.restql.grammar.token;

public class Binary extends Token {

    public Binary(String symbol, int precedence, int rightPrecedence, int nextPrecedence) {
        super(symbol, precedence, rightPrecedence, nextPrecedence);
    }
}
