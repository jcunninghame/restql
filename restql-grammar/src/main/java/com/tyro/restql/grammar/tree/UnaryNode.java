package com.tyro.restql.grammar.tree;

import com.tyro.restql.grammar.token.Token;
import com.tyro.restql.grammar.visitor.QueryVisitor;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class UnaryNode implements Node {

    private final Token token;
    private final Node child;

    public UnaryNode(Token token, Node child) {
        this.token = token;
        this.child = child;
    }

    public Node getChild() {
        return child;
    }

    @Override
    public Token getToken() {
        return token;
    }

    @Override
    public <T> T accept(QueryVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return getToken().toString() + child.toString();
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}
