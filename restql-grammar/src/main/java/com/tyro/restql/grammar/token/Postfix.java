package com.tyro.restql.grammar.token;

public class Postfix extends Token {

    public Postfix(String symbol, int precedence, int rightPrecedence, int nextPrecedence) {
        super(symbol, precedence, rightPrecedence, nextPrecedence);
    }
}
