package com.tyro.restql.grammar.token;

public class GroupOpen extends Token {

    public GroupOpen(String symbol, int precedence, int rightPrecedence, int nextPrecedence) {
        super(symbol, precedence, rightPrecedence, nextPrecedence);
    }
}
