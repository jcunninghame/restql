package com.tyro.restql.grammar.token;

public class Prefix extends Token {

    public Prefix(String symbol, int precedence, int rightPrecedence, int nextPrecedence) {
        super(symbol, precedence, rightPrecedence, nextPrecedence);
    }
}
