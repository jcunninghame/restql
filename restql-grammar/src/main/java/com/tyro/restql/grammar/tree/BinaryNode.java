package com.tyro.restql.grammar.tree;

import com.tyro.restql.grammar.token.Token;
import com.tyro.restql.grammar.visitor.QueryVisitor;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class BinaryNode implements Node {

    private final Token token;
    private final Node leftChild;
    private final Node rightChild;

    public BinaryNode(Token token, Node leftChild, Node rightChild) {
        this.token = token;
        this.leftChild = leftChild;
        this.rightChild = rightChild;
    }

    public Node getLeftChild() {
        return leftChild;
    }

    public Node getRightChild() {
        return rightChild;
    }

    @Override
    public Token getToken() {
        return token;
    }

    @Override
    public <T> T accept(QueryVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return leftChild.toString() + getToken().toString() + rightChild.toString();
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}
