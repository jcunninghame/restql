package com.tyro.restql.grammar.token;

public class GroupClose extends Token {

    public GroupClose(String symbol, int precedence, int rightPrecedence, int nextPrecedence) {
        super(symbol, precedence, rightPrecedence, nextPrecedence);
    }
}
