package com.tyro.restql.grammar;

import com.tyro.restql.grammar.exception.GrammarException;
import com.tyro.restql.grammar.token.Terminal;
import com.tyro.restql.grammar.token.Token;
import com.tyro.restql.grammar.token.Value;
import org.apache.commons.lang3.StringUtils;

import java.util.LinkedList;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class TokenizedQuery {

    private final LinkedList<Token> tokens = new LinkedList<>();

    public TokenizedQuery(String query, Map<String, Supplier<Token>> tokenFactory) {
        int caret = 0;
        boolean escaped = false;
        for (int i = 0; i < query.length(); i++) {
            if (query.charAt(i) == '"') {
                escaped = !escaped;
            }

            String subQuery = query.substring(caret, i + 1);

            Optional<String> symbol = tokenFactory
                    .keySet()
                    .stream()
                    .filter(subQuery::endsWith)
                    .findFirst();

            if (!escaped && symbol.isPresent()) {
                Token token = tokenFactory.get(symbol.get()).get();

                if (caret < i - token.getSymbol().length() + 1) {
                    String value = query.substring(caret, i - token.getSymbol().length() + 1);
                    String removedQuotesValue = trimAndRemoveQuotes(value);

                    if (!StringUtils.isEmpty(removedQuotesValue)) {
                        tokens.add(new Value(removedQuotesValue));
                    }
                }
                tokens.add(token);
                caret = i + (token.getSymbol().endsWith(" ") ? 0 : 1);
            }
        }

        if (caret < query.length()) {
            String value = query.substring(caret);
            String removedQuotesValue = trimAndRemoveQuotes(value);

            if (!StringUtils.isEmpty(removedQuotesValue)) {
                tokens.add(new Value(removedQuotesValue));
            }
        }

        if (escaped) {
            throw new GrammarException("Mismatching escape characters.  Expected [\"] character.");
        }

        tokens.add(new Terminal());
    }

    private String trimAndRemoveQuotes(String value) {
        String trimmedValue = value.trim();
        return trimmedValue.replace("\"", "");
    }

    public Token pop() {
        return tokens.removeFirst();
    }

    public Token pop(Class<? extends Token> type) {
        this.expect(type);
        return pop();
    }

    public Token peek() {
        return tokens.peekFirst();
    }

    public boolean peekType(Class<? extends Token> type) {
        return type.isAssignableFrom(peek().getClass());
    }

    @SafeVarargs
    public final boolean peekType(Class<? extends Token>... types) {
        return Stream.of(types).anyMatch(type -> type.isAssignableFrom(peek().getClass()));
    }

    @SafeVarargs
    public final void expect(Class<? extends Token>... tokenTypes) {
        if (Stream.of(tokenTypes).noneMatch(this::peekType)) {
            throw new GrammarException(this.peek(), tokenTypes);
        }
    }
}
