package com.tyro.restql.grammar.visitor;

import com.tyro.restql.grammar.tree.BinaryNode;
import com.tyro.restql.grammar.tree.UnaryNode;
import com.tyro.restql.grammar.tree.ValueNode;

public interface QueryVisitor<T> {

    T visit(BinaryNode binaryNode);

    T visit(UnaryNode unaryNode);

    T visit(ValueNode valueNode);
}
