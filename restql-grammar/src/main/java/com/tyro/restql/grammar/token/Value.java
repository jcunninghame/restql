package com.tyro.restql.grammar.token;

import java.util.Objects;

public class Value extends Token {

    public Value(String symbol) {
        super(symbol, 0, 0, 0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Value token = (Value) o;
        return Objects.equals(this.getSymbol(), token.getSymbol());
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getSymbol());
    }
}