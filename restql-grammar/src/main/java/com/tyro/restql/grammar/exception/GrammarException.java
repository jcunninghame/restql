package com.tyro.restql.grammar.exception;

import com.tyro.restql.grammar.token.Token;

import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;

public class GrammarException extends IllegalArgumentException {

    public GrammarException(String message) {
        super(message);
    }

    public GrammarException(Token actualToken, Class<? extends Token>... expectedTokens) {
        super(String.format("Illegal token: '%s' of type %s found.  Expected [ %s ]",
                actualToken,
                actualToken.getClass().getSimpleName(),
                Stream.of(expectedTokens)
                        .map(Class::getSimpleName)
                        .collect(joining(" | "))));
    }
}
