package com.tyro.restql.grammar.token;

public class Terminal extends Token {

    public Terminal() {
        super(null, -1, 0, 0);
    }

    @Override
    public String toString() {
        return "TERMINAL";
    }
}