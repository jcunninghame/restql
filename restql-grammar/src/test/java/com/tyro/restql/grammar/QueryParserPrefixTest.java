package com.tyro.restql.grammar;

import com.tyro.restql.grammar.exception.GrammarException;
import com.tyro.restql.grammar.factory.SimpleTokenFactory;
import com.tyro.restql.grammar.tree.Node;
import org.junit.Test;

import static com.tyro.restql.grammar.factory.SimpleTokenFactory.disjunction;
import static com.tyro.restql.grammar.factory.SimpleTokenFactory.negation;
import static com.tyro.restql.grammar.tree.builder.TreeBuilder.BinaryTreeBuilder.binary;
import static com.tyro.restql.grammar.tree.builder.TreeBuilder.UnaryTreeBuilder.unary;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class QueryParserPrefixTest {

    @Test
    public void shouldNegateValueToken() throws Exception {
        String query = "!first";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();

        assertThat(queryTree, is(unary(negation()).child("first")));
    }

    @Test
    public void shouldNegateValueTokenEvenIfTrailing() throws Exception {
        String query = "!first";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();

        assertThat(queryTree, is(unary(negation()).child(("first"))));
    }

    @Test
    public void shouldReturnDisjunctionWithNegatedLeftChild() throws Exception {
        String query = "first !^ forth";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();

        assertThat(queryTree, is(binary(disjunction()).left(unary(negation()).child(("first"))).right(("forth"))));
    }

    @Test(expected = GrammarException.class)
    public void shouldThrowParserExceptionWhenThereIsANegationTokenWithNoPairToModify() throws Exception {
        String query = "!";

        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();
    }
}
