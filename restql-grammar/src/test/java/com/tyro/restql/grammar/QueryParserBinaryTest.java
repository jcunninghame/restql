package com.tyro.restql.grammar;

import com.tyro.restql.grammar.exception.GrammarException;
import com.tyro.restql.grammar.factory.SimpleTokenFactory;
import com.tyro.restql.grammar.tree.Node;
import org.junit.Test;

import static com.tyro.restql.grammar.factory.SimpleTokenFactory.conjunction;
import static com.tyro.restql.grammar.factory.SimpleTokenFactory.disjunction;
import static com.tyro.restql.grammar.tree.builder.TreeBuilder.BinaryTreeBuilder.binary;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class QueryParserBinaryTest {

    @Test
    public void shouldReturnTreeWithDisjunctionOfTwoValues() throws Exception {
        String query = "first^second";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();

        assertThat(queryTree, is(binary(disjunction()).left("first").right("second")));
    }

    @Test
    public void shouldReturnTreeWithConjunctionOfTwoValues() throws Exception {
        String query = "first;second";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();

        assertThat(queryTree, is(binary(conjunction()).left("first").right("second")));
    }

    @Test
    public void shouldReturnTreeWithConjunctionOfAValueAndAConjunctionOfTwoValues() throws Exception {
        String query = "first;second;third";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();

        assertThat(queryTree, is(binary(conjunction()).left(binary(conjunction()).left("first").right("second")).right("third")));
    }

    @Test(expected = GrammarException.class)
    public void shouldThrowParserExceptionWhenUsingBinaryOperatorWithoutLeftChild() throws Exception {
        String query = "^first";

        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();
    }

    @Test(expected = GrammarException.class)
    public void shouldThrowParserExceptionWhenUsingBinaryOperatorWithoutRightChild() throws Exception {
        String query = "first^";

        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();
    }

    @Test
    public void shouldReturnTreeWithDisjunctionOfTwoValuesUsingSpacedSymbol() throws Exception {
        String query1 = "first ^ second";
        String query2 = "first^    second";
        Node queryTree1 = new QueryParser(new TokenizedQuery(query1, new SimpleTokenFactory())).parse();
        Node queryTree2 = new QueryParser(new TokenizedQuery(query2, new SimpleTokenFactory())).parse();

        Node expectedTree = binary(disjunction()).left(("first")).right(("second"));

        assertThat(queryTree1, is(expectedTree));
        assertThat(queryTree2, is(expectedTree));
    }
}
