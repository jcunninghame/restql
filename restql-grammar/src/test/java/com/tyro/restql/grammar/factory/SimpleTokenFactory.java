package com.tyro.restql.grammar.factory;

import com.tyro.restql.grammar.token.*;

import java.util.HashMap;
import java.util.function.Supplier;

public class SimpleTokenFactory extends HashMap<String, Supplier<Token>> {

    public static final String GROUP_OPEN = "(";
    public static final String GROUP_CLOSE = ")";
    public static final String CONJUNCTION = ";";
    public static final String DISJUNCTION = "^";
    public static final String NEGATE = "!";

    public SimpleTokenFactory() {
        this.put(GROUP_OPEN, SimpleTokenFactory::groupOpen);
        this.put(GROUP_CLOSE, SimpleTokenFactory::groupClose);
        this.put(CONJUNCTION, SimpleTokenFactory::conjunction);
        this.put(DISJUNCTION, SimpleTokenFactory::disjunction);
        this.put(NEGATE, SimpleTokenFactory::negation);
    }

    public static GroupOpen groupOpen() {
        return new GroupOpen(GROUP_OPEN, 0, 0, 0);
    }

    public static GroupClose groupClose() {
        return new GroupClose(GROUP_CLOSE, 0, 0, 0);
    }

    public static Binary disjunction() {
        return new Binary(DISJUNCTION, 1, 2, 1);
    }

    public static Binary conjunction() {
        return new Binary(CONJUNCTION, 2, 3, 2);
    }

    public static Prefix negation() {
        return new Prefix(NEGATE, 3, 3, 3);
    }
}
