package com.tyro.restql.grammar;

import com.tyro.restql.grammar.exception.GrammarException;
import com.tyro.restql.grammar.factory.SimpleTokenFactory;
import com.tyro.restql.grammar.tree.Node;
import org.junit.Test;

import static com.tyro.restql.grammar.tree.builder.TreeBuilder.LeafBuilder.value;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class QueryParserEscapeTest {

    @Test
    public void shouldAcceptContainedSpacesForValueTokens() throws Exception {
        String query = "Firstname Lastname";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();

        assertThat(queryTree, is(value("Firstname Lastname")));
    }

    @Test
    public void shouldEscapeReservedTokensInsideDoubleQuotesThenRemoveDoubleQuotes() throws Exception {
        String query = "\"Firstname and Middlename && Lastname\"";

        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();
        assertThat(queryTree, is(value("Firstname and Middlename && Lastname")));
    }

    @Test(expected = GrammarException.class)
    public void shouldthrowGrammerExceptionWhenAnEscapeGroupIsNotCorrectlyClosed() throws Exception {
        String query = "\"Firstname and Middlename && Lastname";
        new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();
    }
}
