package com.tyro.restql.grammar;

import com.tyro.restql.grammar.exception.GrammarException;
import com.tyro.restql.grammar.factory.SimpleTokenFactory;
import com.tyro.restql.grammar.tree.Node;
import org.junit.Test;

import static com.tyro.restql.grammar.factory.SimpleTokenFactory.*;
import static com.tyro.restql.grammar.tree.builder.TreeBuilder.BinaryTreeBuilder.binary;
import static com.tyro.restql.grammar.tree.builder.TreeBuilder.LeafBuilder.value;
import static com.tyro.restql.grammar.tree.builder.TreeBuilder.UnaryTreeBuilder.unary;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class QueryParserGroupsTest {

    @Test
    public void shouldGroupSingleValueAndReturnSingleValueTree() throws Exception {
        String query = "(first)";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();

        assertThat(queryTree, is(value("first")));
    }

    @Test
    public void shouldGroupBinaryValueAndReturnBinaryValueTree() throws Exception {
        String query = "(first ^ second)";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();

        assertThat(queryTree, is(binary(disjunction()).left("first").right("second")));
    }

    @Test
    public void shouldGroupNegationAndReturnNegatedValueTree() throws Exception {
        String query = "(!first)";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();

        assertThat(queryTree, is(unary(negation()).child("first")));
    }

    @Test
    public void shouldNegateGroupOfValueAndReturnNegatedValueTree() throws Exception {
        String query = "!(first)";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();

        assertThat(queryTree, is(unary(negation()).child("first")));
    }

    @Test
    public void shouldEvaluateGroupsWithHighestPrecedence() throws Exception {
        String query = "(first ^ second);third";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();

        assertThat(queryTree, is(
                binary(conjunction())
                        .left(binary(disjunction())
                                .left("first")
                                .right("second"))
                        .right("third")));
    }

    @Test
    public void shouldEvaluateGroupsWithSpacedBrackets() throws Exception {
        String query1 = "( first ^ second ) ; third";
        Node queryTree1 = new QueryParser(new TokenizedQuery(query1, new SimpleTokenFactory())).parse();

        Node expectedTree = binary(conjunction())
                .left(binary(disjunction())
                        .left("first")
                        .right("second"))
                .right("third");

        assertThat(queryTree1, is(expectedTree));
    }

    @Test
    public void shouldEvaluatePrecedenceInOrderOfGroupsThenConjunctionThenDisjunction() throws Exception {
        String query = "(first ; second ^ third) ; forth";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();

        assertThat(queryTree, is(
                binary(conjunction())
                        .left(binary(disjunction())
                                .left(binary(conjunction())
                                        .left("first")
                                        .right("second"))
                                .right("third"))
                        .right("forth")));
    }

    @Test(expected = GrammarException.class)
    public void shouldThrowGrammarExceptionWhenEvaluatingASingleOpenGroup() throws Exception {
        String query = "(";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();
    }

    @Test(expected = GrammarException.class)
    public void shouldThrowGrammarExceptionWhenEvaluatingAnEmptyGroup() throws Exception {
        String query = "()";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();
    }

    @Test(expected = GrammarException.class)
    public void shouldThrowGrammarExceptionWhenEvaluatingAGroupThatIsntClosed() throws Exception {
        String query = "(value";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();
    }

    @Test(expected = GrammarException.class)
    public void shouldThrowGrammarExceptionWhenEvaluatingANestedGroupThatIsntClosed() throws Exception {
        String query = "(left^(right)";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();
    }

    @Test(expected = GrammarException.class)
    public void shouldThrowGrammarExceptionWhenEvaluatingACloseGroupThatDoesntCloseAnOpenGroup() throws Exception {
        String query = "left^(right))";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();
    }
}
