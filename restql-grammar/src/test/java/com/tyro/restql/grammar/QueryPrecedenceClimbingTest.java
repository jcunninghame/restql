package com.tyro.restql.grammar;

import com.tyro.restql.grammar.factory.SimpleTokenFactory;
import com.tyro.restql.grammar.tree.Node;
import org.junit.Test;

import static com.tyro.restql.grammar.factory.SimpleTokenFactory.conjunction;
import static com.tyro.restql.grammar.factory.SimpleTokenFactory.disjunction;
import static com.tyro.restql.grammar.factory.SimpleTokenFactory.negation;
import static com.tyro.restql.grammar.tree.builder.TreeBuilder.BinaryTreeBuilder.binary;
import static com.tyro.restql.grammar.tree.builder.TreeBuilder.UnaryTreeBuilder.unary;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class QueryPrecedenceClimbingTest {

    @Test
    public void shouldGivenConjunctionHigherPrecedenceThanDisjunction() throws Exception {
        String query = "first^second;third";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();

        assertThat(queryTree, is(binary(disjunction()).left("first").right(binary(conjunction()).left("second").right("third"))));
    }

    @Test
    public void shouldEvaluateGroupsWithHighestPrecedence() throws Exception {
        String query = "(first^second);third";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();

        assertThat(queryTree, is(binary(conjunction()).left(binary(disjunction()).left("first").right("second")).right("third")));
    }

    @Test
    public void shouldEvaluatePrecedenceInOrderOfGroupsThenNegationThenConjunctionThenDisjunction() throws Exception {
        String query = "(!first;second^third);forth";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();

        assertThat(queryTree, is(binary(conjunction())
                .left(binary(disjunction())
                        .left(binary(conjunction())
                                .left(unary(negation())
                                        .child("first"))
                                .right("second"))
                        .right("third"))
                .right("forth")
        ));
    }

    @Test
    public void shouldResolveComplicatedExpression() throws Exception {
        String query = "((!(first^second);third)^forth^!fifth);sixth;(!(seventh))";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();

        assertThat(queryTree, is(binary(conjunction())
                .left(binary(conjunction())
                        .left(binary(disjunction())
                                .left(binary(disjunction())
                                        .left(binary(conjunction())
                                                .left(unary(negation())
                                                        .child(binary(disjunction())
                                                                .left("first")
                                                                .right("second")))
                                                .right("third"))
                                        .right("forth"))
                                .right(unary(negation())
                                        .child("fifth")))
                        .right("sixth"))
                .right(unary(negation())
                        .child("seventh"))
        ));
    }
}
