package com.tyro.restql.grammar;

import com.tyro.restql.grammar.factory.SimpleTokenFactory;
import com.tyro.restql.grammar.tree.Node;
import org.junit.Test;

import static com.tyro.restql.grammar.tree.builder.TreeBuilder.LeafBuilder.value;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class QueryParserValueTest {

    @Test
    public void shouldReturnValue() throws Exception {
        String query = "first";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();

        assertThat(queryTree, is(value("first")));
    }

    @Test
    public void shouldTrimLeadingSpacesForValueTokens() throws Exception {
        String query = " first";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();

        assertThat(queryTree, is(value("first")));
    }

    @Test
    public void shouldTrimTrailingSpacesForValueTokens() throws Exception {
        String query = "first  ";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new SimpleTokenFactory())).parse();

        assertThat(queryTree, is(value("first")));
    }
}
