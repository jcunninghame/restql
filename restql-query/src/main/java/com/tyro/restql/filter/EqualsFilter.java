package com.tyro.restql.filter;

import com.tyro.restql.filter.visitor.RestQLVisitor;
import com.tyro.restql.grammar.tree.Node;

public class EqualsFilter<T> extends OperatorFilter<T> {

    public EqualsFilter(String key, String value) {
        super(key, value);
    }

    public EqualsFilter(Node keyNode, Node valueNode) {
        this(keyNode.getToken().getSymbol(), valueNode.getToken().getSymbol());
    }

    @Override
    public <U> U accept(RestQLVisitor<U> visitor) {
        return visitor.visit(this);
    }

    @Override
    public boolean testOperator(Object actual, Object expectation) {
        return actual.equals(expectation);
    }
}
