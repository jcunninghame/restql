package com.tyro.restql.filter.converter;

public interface TypeCoercion {

    boolean canCoerce(Class<?> type);

    <T> T coerce(Class<T> type, String value);
}
