/*
 * Copyright (c) 2003 - 2017 Tyro Payments Limited.
 * Lv1, 155 Clarence St, Sydney NSW 2000.
 * All rights reserved.
 */
package com.tyro.restql.filter.visitor;

import com.tyro.restql.filter.*;

public interface RestQLVisitor<U> {

    <T> U visit(ConjunctionFilter<T> node);

    <T> U visit(DisjunctionFilter<T> node);

    <T> U visit(NegatingFilter<T> node);

    <T> U visit(EqualsFilter<T> node);

    <T> U visit(LikeFilter<T> node);

    <T> U visit(GreaterThanFilter<T> node);

    <T> U visit(LessThanFilter<T> node);

    <T> U visit(HasFilter<T> node);
}
