package com.tyro.restql.filter.factory;

import com.tyro.restql.grammar.token.*;

import java.util.HashMap;
import java.util.function.Supplier;

public class RestQLTokenFactory extends HashMap<String, Supplier<Token>> {

    public static final String GROUP_OPEN = "(";
    public static final String GROUP_CLOSE = ")";
    public static final String CONJUNCTION = "&&";
    public static final String CONJUNCTION_AND = " and ";
    public static final String CONJUNCTION_AND_UPPER = " AND ";
    public static final String DISJUNCTION = "||";
    public static final String DISJUNCTION_OR = " or ";
    public static final String DISJUNCTION_OR_UPPER = " OR ";
    public static final String NEGATE = "!";
    public static final String NOT = " not ";
    public static final String NOT_UPPER = " NOT ";
    public static final String EQUAL = "=";
    public static final String LIKE = "~";
    public static final String GREATER_THAN = ">";
    public static final String LESS_THAN = "<";
    public static final String HAS = " has ";
    public static final String HAS_UPPER = " HAS ";

    public RestQLTokenFactory() {
        this.put(GROUP_OPEN, RestQLTokenFactory::groupOpen);
        this.put(GROUP_CLOSE, RestQLTokenFactory::groupClose);
        this.put(CONJUNCTION, RestQLTokenFactory::conjunction);
        this.put(CONJUNCTION_AND, RestQLTokenFactory::conjunctionAnd);
        this.put(CONJUNCTION_AND_UPPER, RestQLTokenFactory::conjunctionAndUpper);
        this.put(DISJUNCTION, RestQLTokenFactory::disjunction);
        this.put(DISJUNCTION_OR, RestQLTokenFactory::disjunctionOr);
        this.put(DISJUNCTION_OR_UPPER, RestQLTokenFactory::disjunctionOrUpper);
        this.put(NEGATE, RestQLTokenFactory::negation);
        this.put(NOT, RestQLTokenFactory::not);
        this.put(NOT_UPPER, RestQLTokenFactory::not);
        this.put(EQUAL, RestQLTokenFactory::equal);
        this.put(LIKE, RestQLTokenFactory::like);
        this.put(GREATER_THAN, RestQLTokenFactory::greaterThan);
        this.put(LESS_THAN, RestQLTokenFactory::lessThan);
        this.put(HAS, RestQLTokenFactory::has);
        this.put(HAS_UPPER, RestQLTokenFactory::hasUpper);
    }

    public static GroupOpen groupOpen() {
        return new GroupOpen(GROUP_OPEN, 0, 0, 0);
    }

    public static GroupClose groupClose() {
        return new GroupClose(GROUP_CLOSE, 0, 0, 0);
    }

    public static Binary disjunction() {
        return new Binary(DISJUNCTION, 1, 2, 1);
    }

    public static Binary disjunctionOr() {
        return new Binary(DISJUNCTION_OR, 1, 2, 1);
    }

    public static Binary disjunctionOrUpper() {
        return new Binary(DISJUNCTION_OR_UPPER, 1, 2, 1);
    }

    public static Binary conjunction() {
        return new Binary(CONJUNCTION, 2, 3, 2);
    }

    public static Binary conjunctionAnd() {
        return new Binary(CONJUNCTION_AND, 2, 3, 2);
    }

    public static Binary conjunctionAndUpper() {
        return new Binary(CONJUNCTION_AND_UPPER, 2, 3, 2);
    }

    public static Prefix negation() {
        return new Prefix(NEGATE, 4, 4, 4);
    }

    public static Prefix not() {
        return new Prefix(NOT, 4, 4, 4);
    }

    public static Prefix notUpper() {
        return new Prefix(NOT_UPPER, 4, 4, 4);
    }

    public static Binary equal() {
        return new Binary(EQUAL, 4, 4, 3);
    }

    public static Binary like() {
        return new Binary(LIKE, 4, 4, 3);
    }

    public static Binary greaterThan() {
        return new Binary(GREATER_THAN, 4, 4, 3);
    }

    public static Binary lessThan() {
        return new Binary(LESS_THAN, 4, 4, 3);
    }

    public static Binary has() {
        return new Binary(HAS, 4, 4, 3);
    }

    public static Binary hasUpper() {
        return new Binary(HAS_UPPER, 4, 4, 3);
    }

    public static Value value(String value) {
        return new Value(value);
    }

    public static Terminal terminal() {
        return new Terminal();
    }
}
