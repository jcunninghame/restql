/*
 * Copyright (c) 2003 - 2017 Tyro Payments Limited.
 * Lv1, 155 Clarence St, Sydney NSW 2000.
 * All rights reserved.
 */
package com.tyro.restql.filter;

import com.tyro.restql.filter.visitor.RestQLVisitor;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class ConjunctionFilter<T> implements Filter<T> {

    private final Filter<T> leftChild;
    private final Filter<T> rightChild;

    public static <T> ConjunctionFilter and(Filter<T> leftChild, Filter<T> rightChild) {
        return new ConjunctionFilter<>(leftChild, rightChild);
    }

    public ConjunctionFilter(Filter<T> leftChild, Filter<T> rightChild) {
        this.leftChild = leftChild;
        this.rightChild = rightChild;
    }

    public Filter<T> getLeftChild() {
        return leftChild;
    }

    public Filter<T> getRightChild() {
        return rightChild;
    }

    @Override
    public <U> U accept(RestQLVisitor<U> visitor) {
        return visitor.visit(this);
    }

    @Override
    public boolean test(T t) {
        return leftChild.and(rightChild).test(t);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}
