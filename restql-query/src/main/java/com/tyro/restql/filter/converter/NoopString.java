package com.tyro.restql.filter.converter;

public class NoopString implements TypeCoercion {

    @Override
    public boolean canCoerce(Class<?> type) {
        return type.equals(String.class);
    }

    @Override
    public <T> T coerce(Class<T> type, String value) {
        return (T) value;
    }
}
