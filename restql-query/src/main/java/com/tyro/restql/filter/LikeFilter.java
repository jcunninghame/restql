package com.tyro.restql.filter;

import com.tyro.restql.filter.exception.InvalidOperationException;
import com.tyro.restql.filter.visitor.RestQLVisitor;
import com.tyro.restql.grammar.tree.Node;

public class LikeFilter<T> extends OperatorFilter<T> {

    public LikeFilter(String key, String value) {
        super(key, value);
    }

    public LikeFilter(Node keyNode, Node valueNode) {
        this(keyNode.getToken().getSymbol(), valueNode.getToken().getSymbol());
    }

    @Override
    public <U> U accept(RestQLVisitor<U> visitor) {
        return visitor.visit(this);
    }

    @Override
    public boolean testOperator(Object actual, Object expectation) {

        if (actual instanceof String && expectation instanceof String) {
            return ((String) actual).contains((String) expectation);
        }

        throw new InvalidOperationException(this, "Number or Comparable types");
    }
}
