/*
 * Copyright (c) 2003 - 2017 Tyro Payments Limited.
 * Lv1, 155 Clarence St, Sydney NSW 2000.
 * All rights reserved.
 */
package com.tyro.restql.filter.exception;

import com.tyro.restql.filter.OperatorFilter;

import static java.lang.String.format;

public class InvalidOperationException extends RuntimeException {

    public InvalidOperationException(String message) {
        super(message);
    }

    public InvalidOperationException(OperatorFilter filter, String expected) {
        super(format("For key: %s, operation '%s' cannot be applied to %s.  Expected: %s",
                filter.getKey(),
                filter.getClass().getSimpleName(),
                filter.getValue() == null ? "NULL" : filter.getValue().getClass().getSimpleName(),
                expected));
    }
}
