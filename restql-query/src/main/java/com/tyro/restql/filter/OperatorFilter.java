/*
 * Copyright (c) 2003 - 2017 Tyro Payments Limited.
 * Lv1, 155 Clarence St, Sydney NSW 2000.
 * All rights reserved.
 */
package com.tyro.restql.filter;

import com.tyro.restql.filter.converter.Converters;
import com.tyro.restql.grammar.tree.Node;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public abstract class OperatorFilter<T> extends KeyedFilter<T> {

    private final String value;

    public OperatorFilter(String key, String value) {
        super(key);
        this.value = value;
    }

    public OperatorFilter(Node keyNode, Node valueNode) {
        this(keyNode.getToken().getSymbol(), valueNode.getToken().getSymbol());
    }

    public String getValue() {
        return value;
    }

    public <U> U coerceValueTo(Class<U> type) {
        return Converters.coerce(type, value);
    }

    @Override
    public boolean test(T t) {
        return getPathObject(t)
                .map(o -> testOperator(o, coerceValueTo(o.getClass())))
                .orElse(false);
    }

    public abstract boolean testOperator(Object actual, Object expectation);

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}
