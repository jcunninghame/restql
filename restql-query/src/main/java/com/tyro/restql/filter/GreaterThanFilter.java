package com.tyro.restql.filter;

import com.tyro.restql.filter.exception.InvalidOperationException;
import com.tyro.restql.filter.visitor.RestQLVisitor;
import com.tyro.restql.grammar.tree.Node;

import java.math.BigDecimal;

public class GreaterThanFilter<T> extends OperatorFilter<T> {

    public GreaterThanFilter(String key, String value) {
        super(key, value);
    }

    public GreaterThanFilter(Node keyNode, Node valueNode) {
        this(keyNode.getToken().getSymbol(), valueNode.getToken().getSymbol());
    }

    @Override
    public <U> U accept(RestQLVisitor<U> visitor) {
        return visitor.visit(this);
    }

    @Override
    public boolean testOperator(Object actual, Object expectation) {

        if (actual instanceof Comparable && expectation instanceof Comparable) {
            return ((Comparable) actual).compareTo(expectation) > 0;
        }

        if (actual instanceof Number && expectation instanceof Number) {
            return BigDecimal.valueOf(((Number) actual).longValue())
                    .compareTo(BigDecimal.valueOf(((Number) actual).longValue())) > 0;
        }

        throw new InvalidOperationException(this, "Number or Comparable types");
    }
}
