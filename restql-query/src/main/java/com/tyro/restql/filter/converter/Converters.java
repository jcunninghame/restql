package com.tyro.restql.filter.converter;

import java.util.List;

import static java.lang.String.format;
import static java.util.Arrays.asList;

public class Converters {

    private static final List<TypeCoercion> TYPE_COERCIONS = asList(
            new NoopString(),
            new InvokeParse(),
            new InvokeValueOf()
    );

    public static <T> T coerce(Class<T> type, String value) {
        return TYPE_COERCIONS
                .stream()
                .filter(tc -> tc.canCoerce(type))
                .findFirst()
                .map(tc -> tc.coerce(type, value))
                .orElseThrow(() ->
                        new IllegalArgumentException(format("Could not coerce value %s to type %s ", value, type)));
    }
}
