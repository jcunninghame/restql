package com.tyro.restql.filter;

import java.lang.reflect.Field;
import java.util.Optional;

public abstract class KeyedFilter<T> implements Filter<T> {

    private final String key;

    public KeyedFilter(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public String[] getKeyPath() {
        return key.split("\\.");
    }

    protected Optional<Object> getPathObject(T t) {
        Object obj = t;
        for (String fieldKey : getKeyPath()) {
            Optional<Field> field = getField(fieldKey, obj.getClass());
            if (!field.isPresent()) {
                return Optional.empty();
            }

            Optional<Object> fieldValue = getFieldValue(obj, field.get());
            if (!fieldValue.isPresent()) {
                return Optional.empty();
            }

            obj = fieldValue.get();
        }
        return Optional.of(obj);
    }

    private static Optional<Object> getFieldValue(Object obj, Field field) {
        try {
            field.setAccessible(true);
            return Optional.of(field.get(obj));
        } catch (IllegalAccessException e) {
            return Optional.empty();
        }
    }

    private static Optional<Field> getField(String key, Class<?> type) {
        if (type != null) {
            try {
                return Optional.of(type.getDeclaredField(key));
            } catch (NoSuchFieldException e) {
                return getField(key, type.getSuperclass());
            }
        }
        return Optional.empty();
    }
}
