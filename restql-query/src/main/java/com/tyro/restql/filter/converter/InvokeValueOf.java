package com.tyro.restql.filter.converter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class InvokeValueOf implements TypeCoercion {

    private String methodName;

    public InvokeValueOf() {
        this.methodName = "valueOf";
    }

    public boolean canCoerce(Class<?> type) {
        try {
            Method method = type.getMethod(methodName, String.class);
            return method != null;
        } catch (NoSuchMethodException e) {
            return false;
        }
    }

    public <T> T coerce(Class<T> type, String value) {
        try {
            Method method = type.getMethod(methodName, String.class);
            method.setAccessible(true);
            return (T) method.invoke(type, value);
        } catch (NoSuchMethodException
                | InvocationTargetException
                | IllegalAccessException e) {
            return null;
        }
    }
}
