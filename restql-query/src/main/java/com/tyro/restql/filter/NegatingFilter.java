/*
 * Copyright (c) 2003 - 2017 Tyro Payments Limited.
 * Lv1, 155 Clarence St, Sydney NSW 2000.
 * All rights reserved.
 */
package com.tyro.restql.filter;

import com.tyro.restql.filter.visitor.RestQLVisitor;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class NegatingFilter<T> implements Filter<T> {

    private final Filter<T> child;

    public NegatingFilter(Filter<T> child) {
        this.child = child;
    }

    public Filter<T> getChild() {
        return child;
    }

    @Override
    public <U> U accept(RestQLVisitor<U> visitor) {
        return visitor.visit(this);
    }

    @Override
    public boolean test(T t) {
        return !child.test(t);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}
