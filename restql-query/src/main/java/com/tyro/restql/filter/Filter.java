/*
 * Copyright (c) 2003 - 2017 Tyro Payments Limited.
 * Lv1, 155 Clarence St, Sydney NSW 2000.
 * All rights reserved.
 */
package com.tyro.restql.filter;

import com.tyro.restql.filter.visitor.RestQLVisitor;

import java.util.function.Predicate;

public interface Filter<T> extends Predicate<T> {

    <U> U accept(RestQLVisitor<U> visitor);
}
