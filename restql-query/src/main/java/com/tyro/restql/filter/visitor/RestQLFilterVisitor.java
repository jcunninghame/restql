package com.tyro.restql.filter.visitor;

import com.tyro.restql.filter.*;
import com.tyro.restql.grammar.token.Token;
import com.tyro.restql.grammar.tree.BinaryNode;
import com.tyro.restql.grammar.tree.UnaryNode;
import com.tyro.restql.grammar.tree.ValueNode;
import com.tyro.restql.grammar.visitor.QueryVisitor;

import static com.tyro.restql.filter.factory.RestQLTokenFactory.*;

public class RestQLFilterVisitor<T> implements QueryVisitor<Filter<T>> {

    @Override
    public Filter<T> visit(BinaryNode binaryNode) {
        Token token = binaryNode.getToken();

        switch (token.getSymbol()) {
            case CONJUNCTION : case CONJUNCTION_AND : case CONJUNCTION_AND_UPPER :
                return new ConjunctionFilter<>(
                    binaryNode.getLeftChild().accept(this),
                    binaryNode.getRightChild().accept(this));
            case DISJUNCTION : case DISJUNCTION_OR : case DISJUNCTION_OR_UPPER :
                return new DisjunctionFilter<>(
                        binaryNode.getLeftChild().accept(this),
                        binaryNode.getRightChild().accept(this));
            case HAS: case HAS_UPPER:
                return new HasFilter<>(
                        binaryNode.getLeftChild(),
                        binaryNode.getRightChild().accept(this));
            case LESS_THAN :
                return new LessThanFilter<>(binaryNode.getLeftChild(), binaryNode.getRightChild());
            case GREATER_THAN :
                return new GreaterThanFilter<>(binaryNode.getLeftChild(), binaryNode.getRightChild());
            case EQUAL :
                return new EqualsFilter<>(binaryNode.getLeftChild(), binaryNode.getRightChild());
            case LIKE :
                return new LikeFilter<>(binaryNode.getLeftChild(), binaryNode.getRightChild());
            default : throw new UnsupportedOperationException();
        }
    }

    @Override
    public Filter<T> visit(UnaryNode unaryNode) {
        Token token = unaryNode.getToken();

        switch (token.getSymbol()) {
            case NEGATE : case NOT : case NOT_UPPER :
                return new NegatingFilter<>(unaryNode.getChild().accept(this));
            default : throw new UnsupportedOperationException();
        }
    }

    @Override
    public Filter<T> visit(ValueNode valueNode) {
        throw new UnsupportedOperationException();
    }
}
