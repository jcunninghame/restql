package com.tyro.restql.filter;

import com.tyro.restql.filter.visitor.RestQLVisitor;
import com.tyro.restql.grammar.tree.Node;

import java.util.Collection;

public class HasFilter<T> extends KeyedFilter<T> {

    private final Filter<T> value;

    public HasFilter(Node key, Filter<T> value) {
        super(key.getToken().getSymbol());
        this.value = value;
    }

    public Filter<T> getValue() {
        return value;
    }

    @Override
    public boolean test(T t) {
        return getPathObject(t)
                .map(o -> ((Collection<?>) o)
                        .stream()
                        .anyMatch(v -> value.test((T) v)))
                .orElse(false);
    }

    @Override
    public <U> U accept(RestQLVisitor<U> visitor) {
        return visitor.visit(this);
    }
}