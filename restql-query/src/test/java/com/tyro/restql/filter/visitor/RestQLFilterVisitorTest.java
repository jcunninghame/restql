package com.tyro.restql.filter.visitor;

import com.tyro.restql.filter.*;
import com.tyro.restql.grammar.tree.Node;
import org.junit.Test;

import static com.tyro.restql.filter.factory.RestQLTokenFactory.*;
import static com.tyro.restql.grammar.tree.builder.TreeBuilder.BinaryTreeBuilder.binary;
import static com.tyro.restql.grammar.tree.builder.TreeBuilder.UnaryTreeBuilder.unary;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class RestQLFilterVisitorTest {

    private static final RestQLFilterVisitor<?> VISITOR = new RestQLFilterVisitor();

    @Test
    public void shouldTransformEqualsQueryNodeToEqualsFilter() throws Exception {
        Filter expectedFilter = new EqualsFilter("key", "value");

        Node queryNode = binary(equal()).left("key").right("value");
        Filter filter = queryNode.accept(VISITOR);

        assertThat(filter, is(expectedFilter));
    }

    @Test
    public void shouldTransformLikeQueryNodeToLikeFilter() throws Exception {
        Filter expectedFilter = new LikeFilter("key", "value");

        Node queryNode = binary(like()).left("key").right("value");
        Filter filter = queryNode.accept(VISITOR);

        assertThat(filter, is(expectedFilter));
    }

    @Test
    public void shouldTransformGreaterThanQueryNodeToGreaterThanFilter() throws Exception {
        Filter expectedFilter = new GreaterThanFilter("key", "value");

        Node queryNode = binary(greaterThan()).left("key").right("value");
        Filter filter = queryNode.accept(VISITOR);

        assertThat(filter, is(expectedFilter));
    }

    @Test
    public void shouldTransformLessThanQueryNodeToLessThanFilter() throws Exception {
        Filter expectedFilter = new LessThanFilter("key", "value");

        Node queryNode = binary(lessThan()).left("key").right("value");
        Filter filter = queryNode.accept(VISITOR);

        assertThat(filter, is(expectedFilter));
    }

    @Test
    public void shouldTransformNegatingQueryNodeToNegatingFilter() throws Exception {
        Filter expectedFilter = new NegatingFilter<>(new EqualsFilter<>("key", "value"));

        Node queryNode = unary(negation()).child(binary(equal()).left("key").right("value"));
        Filter filter = queryNode.accept(VISITOR);

        assertThat(filter, is(expectedFilter));
    }

    @Test
    public void shouldTransformConjunctionQueryNodeToConjunctionAndFilter() throws Exception {
        Filter simpleFilterOne = new EqualsFilter("key1", "value");
        Filter simpleFilterTwo = new GreaterThanFilter("key2", "value");
        Filter expectedFilter = new ConjunctionFilter(simpleFilterOne, simpleFilterTwo);

        Node queryNode = binary(conjunctionAnd())
                .left(binary(equal())
                        .left("key1")
                        .right("value"))
                .right(binary(greaterThan())
                        .left("key2")
                        .right("value"));

        Filter filter = queryNode.accept(VISITOR);

        assertThat(filter, is(expectedFilter));
    }

    @Test
    public void shouldTransformDisjunctionQueryNodeToDisjunctionOrFilter() throws Exception {
        Filter simpleFilterOne = new EqualsFilter("key1", "value");
        Filter simpleFilterTwo = new GreaterThanFilter("key2", "value");
        Filter expectedFilter = new DisjunctionFilter(simpleFilterOne, simpleFilterTwo);

        Node queryNode = binary(disjunctionOr())
                .left(binary(equal())
                        .left("key1")
                        .right("value"))
                .right(binary(greaterThan())
                        .left("key2")
                        .right("value"));

        Filter filter = queryNode.accept(VISITOR);

        assertThat(filter, is(expectedFilter));
    }
}