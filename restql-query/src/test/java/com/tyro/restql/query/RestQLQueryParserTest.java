package com.tyro.restql.query;

import com.tyro.restql.filter.factory.RestQLTokenFactory;
import com.tyro.restql.grammar.QueryParser;
import com.tyro.restql.grammar.TokenizedQuery;
import com.tyro.restql.grammar.tree.BinaryNode;
import com.tyro.restql.grammar.tree.Node;
import com.tyro.restql.grammar.tree.UnaryNode;
import org.junit.Test;

import static com.tyro.restql.filter.factory.RestQLTokenFactory.*;
import static com.tyro.restql.grammar.tree.builder.TreeBuilder.BinaryTreeBuilder.binary;
import static com.tyro.restql.grammar.tree.builder.TreeBuilder.UnaryTreeBuilder.unary;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class RestQLQueryParserTest {

    @Test
    public void shouldNegateValueToken() throws Exception {
        String query = "!first";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new RestQLTokenFactory())).parse();

        assertThat(queryTree, is(unary(negation()).child("first")));
    }

    @Test
    public void shouldNegateValueTokenWithPostfix() throws Exception {
        String query = "first!";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new RestQLTokenFactory())).parse();

        assertThat(queryTree, is(unary(negation()).child(("first"))));
    }

    @Test
    public void shouldEvaluateEqualityAsBinaryTreeWithTwoValues() throws Exception {
        String query = "valueOne=1";

        Node queryTree = new QueryParser(new TokenizedQuery(query, new RestQLTokenFactory())).parse();

        assertThat(queryTree, is(binary(equal()).left("valueOne").right("1")));
    }


    @Test
    public void shouldEvaluateEquivalencyAsBinaryTreeWithTwoValues() throws Exception {
        String query = "valueOne~1";

        Node queryTree = new QueryParser(new TokenizedQuery(query, new RestQLTokenFactory())).parse();

        assertThat(queryTree, is(binary(like()).left("valueOne").right("1")));
    }

    @Test
    public void shouldEvaluateInequalityAsNegatedBinaryTreeWithTwoValues() throws Exception {
        String query1 = "valueOne!=1";
        String query2 = "!(valueOne=1)";

        Node queryTree1 = new QueryParser(new TokenizedQuery(query1, new RestQLTokenFactory())).parse();
        Node queryTree2 = new QueryParser(new TokenizedQuery(query2, new RestQLTokenFactory())).parse();

        UnaryNode expectedQueryTree = unary(negation()).child(binary(equal()).left("valueOne").right("1"));

        assertThat(queryTree1, is(expectedQueryTree));
        assertThat(queryTree2, is(expectedQueryTree));
    }

    @Test
    public void shouldEvaluateInequivalencyAsNegatedBinaryTreeWithTwoValues() throws Exception {
        String query1 = "valueOne!~1";
        String query2 = "!(valueOne~1)";

        Node queryTree1 = new QueryParser(new TokenizedQuery(query1, new RestQLTokenFactory())).parse();
        Node queryTree2 = new QueryParser(new TokenizedQuery(query2, new RestQLTokenFactory())).parse();

        UnaryNode expectedQueryTree = unary(negation()).child(binary(like()).left("valueOne").right("1"));

        assertThat(queryTree1, is(expectedQueryTree));
        assertThat(queryTree2, is(expectedQueryTree));
    }

    @Test
    public void shouldEvaluateGreaterThanAsGreaterThanBinaryTreeWithTwoValues() throws Exception {
        String query = "valueOne>1";

        Node queryTree = new QueryParser(new TokenizedQuery(query, new RestQLTokenFactory())).parse();

        assertThat(queryTree, is(binary(greaterThan()).left("valueOne").right("1")));
    }

    @Test
    public void shouldEvaluateLessThanOrEqualAsLessThanBinaryTreeWithTwoValues() throws Exception {
        String query = "valueOne<1";

        Node queryTree = new QueryParser(new TokenizedQuery(query, new RestQLTokenFactory())).parse();

        assertThat(queryTree, is(binary(lessThan()).left("valueOne").right("1")));
    }

    @Test
    public void shouldEvaluateConjunctionWithHigherPrecedenceThanEqualityOperators() throws Exception {
        String query = "valueOne=1 and valueTwo<2";

        Node queryTree = new QueryParser(new TokenizedQuery(query, new RestQLTokenFactory())).parse();

        assertThat(queryTree, is(
                binary(conjunction())
                    .left(binary(equal())
                        .left("valueOne")
                        .right("1"))
                    .right(binary(lessThan())
                        .left("valueTwo")
                        .right("2"))
        ));
    }

    @Test
    public void shouldEvaluateDisjunctionWithHigherPrecidenceThanEqualityOperators() throws Exception {
        String query = "valueOne=1 || valueTwo<2";

        Node queryTree = new QueryParser(new TokenizedQuery(query, new RestQLTokenFactory())).parse();

        assertThat(queryTree, is(
                binary(disjunction())
                    .left(binary(equal())
                        .left("valueOne")
                        .right("1"))
                    .right(binary(lessThan())
                        .left("valueTwo")
                        .right("2"))
        ));
    }

    @Test
    public void shouldEvaluateConjunctionWithHigherPrecidenceThanNegationOperators() throws Exception {
        String query = "valueOne!=1 || valueTwo!<2";

        Node queryTree = new QueryParser(new TokenizedQuery(query, new RestQLTokenFactory())).parse();

        assertThat(queryTree, is(
                binary(disjunction())
                    .left(unary(negation())
                        .child(binary(equal())
                            .left("valueOne")
                            .right("1")))
                    .right(unary(negation())
                        .child(binary(lessThan())
                            .left("valueTwo")
                            .right("2")))
        ));
    }

    @Test
    public void shouldAllowNestedNegationNodesNegation() throws Exception {
        String query = "!(valueOne!=1)";

        Node queryTree = new QueryParser(new TokenizedQuery(query, new RestQLTokenFactory())).parse();

        assertThat(queryTree, is(
                unary(negation())
                    .child(unary(negation())
                        .child(binary(equal())
                            .left("valueOne")
                            .right("1")))
        ));
    }

    @Test
    public void shouldReturnTreeWithDisjunctionOfTwoValues() throws Exception {
        String query = "first||second";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new RestQLTokenFactory())).parse();

        assertThat(queryTree, is(binary(disjunction()).left(("first")).right(("second"))));
    }

    @Test
    public void shouldReturnTreeWithDisjunctionOfTwoValuesUsingOrSymbol() throws Exception {
        String query1 = "first or forth";
        String query2 = "first OR forth";
        Node queryTree1 = new QueryParser(new TokenizedQuery(query1, new RestQLTokenFactory())).parse();
        Node queryTree2 = new QueryParser(new TokenizedQuery(query2, new RestQLTokenFactory())).parse();

        Node expectedTree = binary(disjunction()).left(("first")).right(("forth"));

        assertThat(queryTree1, is(expectedTree));
        assertThat(queryTree2, is(expectedTree));
    }

    @Test
    public void shouldReturnTreeWithConjunctionOfTwoValues() throws Exception {
        String query = "first&&second";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new RestQLTokenFactory())).parse();

        assertThat(queryTree, is(binary(conjunction()).left(("first")).right(("second"))));
    }

    @Test
    public void shouldReturnTreeWithConjunctionOfTwoValuesUsingAndSymbol() throws Exception {
        String query1 = "first and second";
        String query2 = "first AND second";
        Node queryTree1 = new QueryParser(new TokenizedQuery(query1, new RestQLTokenFactory())).parse();
        Node queryTree2 = new QueryParser(new TokenizedQuery(query2, new RestQLTokenFactory())).parse();

        BinaryNode expectedTree = binary(conjunction()).left(("first")).right(("second"));

        assertThat(queryTree1, is(expectedTree));
        assertThat(queryTree2, is(expectedTree));
    }

    @Test
    public void shouldReturnTreeWithConjunctionOfAValueAndAConjunctionOfTwoValues() throws Exception {
        String query = "first and second and third";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new RestQLTokenFactory())).parse();

        assertThat(queryTree, is(binary(conjunction())
                .left(binary(conjunction())
                    .left(("first"))
                    .right(("second")))
                .right("third")));
    }

    @Test
    public void shouldReturnConjunctionWithNegatedLeftChild() throws Exception {
        String query = "first !&& forth";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new RestQLTokenFactory())).parse();

        assertThat(queryTree, is(binary(conjunction())
                .left(unary(negation())
                    .child(("first")))
                .right("forth")));
    }

    @Test
    public void shouldGivenConjunctionHigherPrecedenceThanDisjunction() throws Exception {
        String query = "first or second and third";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new RestQLTokenFactory())).parse();

        assertThat(queryTree, is(binary(disjunction())
                .left("first")
                .right(binary(conjunction())
                    .left("second")
                    .right("third"))));
    }

    @Test
    public void shouldEvaluateGroupsWithHighestPrecedence() throws Exception {
        String query = "(first or second) and third";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new RestQLTokenFactory())).parse();

        assertThat(queryTree, is(
                binary(conjunction())
                    .left(binary(disjunction())
                        .left("first")
                        .right("second"))
                    .right("third")));
    }

    @Test
    public void shouldEvaluatePrecedenceInOrderOfGroupsThenConjunctionThenDisjunction() throws Exception {
        String query = "(first and second or third) and forth";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new RestQLTokenFactory())).parse();

        assertThat(queryTree, is(
                binary(conjunction())
                    .left(binary(disjunction())
                        .left(binary(conjunction())
                            .left("first")
                            .right("second"))
                        .right("third"))
                    .right("forth")));
    }

    @Test
    public void shouldReturnHasNodeWithLeftValueNodeAndRightEqualsNode() throws Exception {
        String query = "students has firstname = alice";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new RestQLTokenFactory())).parse();

        assertThat(queryTree, is(
                binary(has())
                    .left("students")
                    .right(binary(equal())
                        .left("firstname")
                        .right("alice"))));
    }

    @Test
    public void shouldReturnNestedHasNodes() throws Exception {
        String query = "classes has students has firstname = alice";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new RestQLTokenFactory())).parse();

        assertThat(queryTree, is(
            binary(has())
                .left("classes")
                .right(binary(has())
                    .left("students")
                    .right(binary(equal())
                        .left("firstname")
                        .right("alice")))));
    }

    @Test
    public void shouldResolveComplicatedExpression() throws Exception {
        String query = "firstname = \"john or johnny\" and allergies not has nuts or (dob !> 21-06-1985 and !enrolled)";
        Node queryTree = new QueryParser(new TokenizedQuery(query, new RestQLTokenFactory())).parse();

        Node notEnrolled = unary(negation()).child("enrolled");
        Node dobNotBefore21Jun1985 = unary(negation()).child(binary(greaterThan()).left("dob").right("21-06-1985"));
        Node firstNameIsJohnOrJohnny = binary(equal()).left("firstname").right("john or johnny");
        Node noNutAllergy = unary(negation()).child(binary(has()).left("allergies").right("nuts"));

        assertThat(queryTree, is(binary(disjunction())
                .left(binary(conjunction())
                    .left(firstNameIsJohnOrJohnny)
                    .right(noNutAllergy))
                .right(binary(conjunction())
                    .left(dobNotBefore21Jun1985)
                    .right(notEnrolled))));
    }
}