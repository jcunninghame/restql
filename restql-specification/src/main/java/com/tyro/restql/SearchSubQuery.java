/*
 * Copyright (c) 2003 - 2017 Tyro Payments Limited.
 * Lv1, 155 Clarence St, Sydney NSW 2000.
 * All rights reserved.
 */
package com.tyro.restql;

import com.tyro.restql.filter.*;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.From;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;

public class SearchSubQuery<U> extends SearchQuery<U> {

    private Path<U> queryPath;

    public SearchSubQuery(From root, KeyedFilter filter) {
        this.queryPath = getKeyPath(root, filter);
    }

    @Override
    public <T> Specification<U> visit(EqualsFilter<T> node) {
        return (root, query, builder) -> new PredicateBuilder<U, T>(queryPath, builder).equal(node);
    }

    @Override
    public <T> Specification<U> visit(LikeFilter<T> node) {
        return (root, query, builder) -> new PredicateBuilder<U, T>(queryPath, builder).like(node);
    }

    @Override
    public <T> Specification<U> visit(GreaterThanFilter<T> node) {
        return (root, query, builder) -> new PredicateBuilder<U, T>(queryPath, builder).greaterThan(node);
    }

    @Override
    public <T> Specification<U> visit(LessThanFilter<T> node) {
        return (root, query, builder) -> new PredicateBuilder<U, T>(queryPath, builder).lessThan(node);
    }

    @Override
    public <T> Specification<U> visit(HasFilter<T> node) {
        return (root, query, builder) -> node.getValue()
                .accept(new SearchSubQuery<U>(root, node))
                .toPredicate(root, query.distinct(true), builder);
    }

    private From getKeyPath(From root, KeyedFilter filter) {
        From path = root;
        for (String pathFragment : filter.getKeyPath()) {
            path = path.join(pathFragment, JoinType.INNER);
        }
        return path;
    }
}
