/*
 * Copyright (c) 2003 - 2017 Tyro Payments Limited.
 * Lv1, 155 Clarence St, Sydney NSW 2000.
 * All rights reserved.
 */
package com.tyro.restql;

import com.tyro.restql.filter.OperatorFilter;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

public class PredicateBuilder<T, U> {

    private final Path<T> root;
    private final CriteriaBuilder builder;

    public PredicateBuilder(Path<T> root, CriteriaBuilder builder) {
        this.root = root;
        this.builder = builder;
    }

    public Predicate equal(OperatorFilter<U> filter) {
        Path<?> keyPath = getKeyPath(filter);

        if (filter.getValue() == null) {
            return builder.isNull(keyPath);
        }
        return builder.equal(keyPath, filter.coerceValueTo(keyPath.getJavaType()));
    }

    public Predicate notEqual(OperatorFilter<U> filter) {
        Path<?> keyPath = getKeyPath(filter);

        if (filter.getValue() == null) {
            return builder.isNotNull(keyPath);
        }
        return builder.notEqual(keyPath, filter.coerceValueTo(keyPath.getJavaType()));
    }

    public Predicate like(OperatorFilter<U> filter) {
        Path keyPath = getKeyPath(filter);

        return builder.like(keyPath, "%" + filter.coerceValueTo(String.class) + "%");
    }

    public Predicate notLike(OperatorFilter<U> filter) {
        Path keyPath = getKeyPath(filter);

        return builder.notLike(keyPath, "%" + filter.coerceValueTo(String.class) + "%");
    }

    public Predicate greaterThan(OperatorFilter<U> filter) {
        Path keyPath = getKeyPath(filter);

        if (Number.class.isAssignableFrom(keyPath.getJavaType())) {
            return builder.gt(keyPath, (Number) filter.coerceValueTo(keyPath.getJavaType()));
        }

        return builder.greaterThan(keyPath, (Comparable) filter.coerceValueTo(keyPath.getJavaType()));
    }

    public Predicate greaterThanOrEqual(OperatorFilter<U> filter) {
        Path keyPath = getKeyPath(filter);

        if (Number.class.isAssignableFrom(keyPath.getJavaType())) {
            return builder.ge(keyPath, (Number) filter.coerceValueTo(keyPath.getJavaType()));
        }
        return builder.greaterThanOrEqualTo(keyPath, (Comparable) filter.coerceValueTo(keyPath.getJavaType()));
    }

    public Predicate lessThan(OperatorFilter<U> filter) {
        Path keyPath = getKeyPath(filter);

        if (Number.class.isAssignableFrom(keyPath.getJavaType())) {
            return builder.lt(keyPath, (Number) filter.coerceValueTo((keyPath.getJavaType())));
        }
        return builder.lessThan(keyPath, (Comparable) filter.coerceValueTo(keyPath.getJavaType()));
    }

    public Predicate lessThanOrEqual(OperatorFilter<U> filter) {
        Path keyPath = getKeyPath(filter);

        if (Number.class.isAssignableFrom(keyPath.getJavaType())) {
            return builder.le(keyPath, (Number) filter.coerceValueTo(keyPath.getJavaType()));
        }
        return builder.lessThanOrEqualTo(keyPath, (Comparable) filter.coerceValueTo(keyPath.getJavaType()));
    }

    public Predicate conjunction() {
        return builder.conjunction();
    }

    public Predicate disjunction() {
        return builder.disjunction();
    }

    private Path getKeyPath(OperatorFilter<U> filter) {
        Path<?> path = root;
        for (String pathFragment : filter.getKeyPath()) {
            path = path.get(pathFragment);
        }
        return path;
    }
}
