/*
 * Copyright (c) 2003 - 2017 Tyro Payments Limited.
 * Lv1, 155 Clarence St, Sydney NSW 2000.
 * All rights reserved.
 */
package com.tyro.restql;

import com.tyro.restql.filter.*;
import com.tyro.restql.filter.visitor.RestQLVisitor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

public class SearchQuery<U> implements RestQLVisitor<Specification<U>> {

    @Override
    public <T> Specification<U> visit(ConjunctionFilter<T> node) {
        Specification<U> leftResult = node.getLeftChild().accept(this);
        Specification<U> rightResult = node.getRightChild().accept(this);
        return Specifications.where(leftResult).and(rightResult);
    }

    @Override
    public <T> Specification<U> visit(DisjunctionFilter<T> node) {
        Specification<U> leftResult = node.getLeftChild().accept(this);
        Specification<U> rightResult = node.getRightChild().accept(this);
        return Specifications.where(leftResult).or(rightResult);
    }

    @Override
    public <T> Specification<U> visit(NegatingFilter<T> node) {
        Specification<U> result = node.getChild().accept(this);
        return Specifications.not(result);
    }

    @Override
    public <T> Specification<U> visit(EqualsFilter<T> node) {
        return (root, query, builder) -> new PredicateBuilder<U, T>(root, builder).equal(node);
    }

    @Override
    public <T> Specification<U> visit(LikeFilter<T> node) {
        return (root, query, builder) -> new PredicateBuilder<U, T>(root, builder).like(node);
    }

    @Override
    public <T> Specification<U> visit(GreaterThanFilter<T> node) {
        return (root, query, builder) -> new PredicateBuilder<U, T>(root, builder).greaterThan(node);
    }

    @Override
    public <T> Specification<U> visit(LessThanFilter<T> node) {
        return (root, query, builder) -> new PredicateBuilder<U, T>(root, builder).lessThan(node);
    }

    @Override
    public <T> Specification<U> visit(HasFilter<T> node) {
        return (root, query, builder) -> node.getValue()
                .accept(new SearchSubQuery<U>(root, node))
                .toPredicate(root, query.distinct(true), builder);
    }
}
