package com.tyro.restql;

import com.tyro.restql.filter.Filter;
import com.tyro.restql.filter.factory.RestQLTokenFactory;
import com.tyro.restql.filter.visitor.RestQLFilterVisitor;
import com.tyro.restql.grammar.QueryParser;
import com.tyro.restql.grammar.TokenizedQuery;
import com.tyro.restql.repo.CourseRepository;
import com.tyro.restql.repo.ProfessorRepository;
import com.tyro.restql.repo.StudentRepository;
import com.tyro.restql.repo.model.Course;
import com.tyro.restql.repo.model.Professor;
import com.tyro.restql.repo.model.Student;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.List;

import static com.tyro.restql.EntityFactory.*;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

@Transactional
@SpringBootTest
@RunWith(SpringRunner.class)
public class RestQLLanguageTest {

    private static final String ALICE_ID = "U_4518510";

    private Student ALICE;
    private Student MICHAEL;
    private Student ALEXANDER;
    private Professor BRADFORD;
    private Professor PROFESSORSON;
    private Course LADDERS;
    private Course STUDIOLOGY;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private ProfessorRepository professorRepository;

    @Autowired
    private CourseRepository courseRepository;

    @Before
    public void setupData() throws Exception {
        ALICE = studentRepository.save(alice());
        MICHAEL = studentRepository.save(michael());
        ALEXANDER = studentRepository.save(alexander());
        BRADFORD = professorRepository.save(bradford());
        PROFESSORSON = professorRepository.save(professorson());
        LADDERS = courseRepository.save(ladders(BRADFORD));
        STUDIOLOGY = courseRepository.save(studiology(PROFESSORSON));
        LADDERS.enroll(ALICE);
        STUDIOLOGY.enroll(ALICE);
        STUDIOLOGY.enroll(MICHAEL);
        STUDIOLOGY.enroll(ALEXANDER);
    }

    @Test
    public void shouldConvertQueryStringIntoSpecificationForStudentAlice() throws Exception {
        String query = "first = Alice and last = Nicholson and year < 9 and birthdate > 1980-01-01";

        List<Student> students = studentRepository.findAll(getSpecification(getFilter(query)));

        assertThat(students, hasSize(1));
        assertThat(students, hasItem(studentRepository.findByStudentId(ALICE_ID)));
    }

    @Test
    public void shouldConvertQueryStringIntoPredicateForStudentAlice() throws Exception {
        String query = "first = Alice and last = Nicholson and year < 9 and birthdate > 1980-01-01";

        List<Student> allStudents = asList(ALICE, MICHAEL, ALEXANDER);

        Filter<Student> filter = getFilter(query);

        List<Student> students = allStudents
                .stream()
                .filter(filter)
                .collect(toList());

        assertThat(students, hasSize(1));
        assertThat(students, hasItem(studentRepository.findByStudentId(ALICE_ID)));
    }

    @Test
    public void shouldReturnAllCoursesWhoAreTaughtByAProfessorWithLastNameBradford() throws Exception {
        String query = "professor.last = Bradford";

        List<Course> courses = courseRepository.findAll(getSpecification(getFilter(query)));

        assertThat(courses, hasSize(1));
        assertThat(courses, hasItem(LADDERS));
    }

    @Test
    public void shouldReturnAllCoursesWhoAreTaughtByProfessorWithABirthdayIn1968() throws Exception {
        String query = "professor.birthdate > 1968-01-01 and professor.birthdate < 1968-12-31";

        List<Course> courses = courseRepository.findAll(getSpecification(getFilter(query)));

        assertThat(courses, hasSize(1));
        assertThat(courses, hasItem(LADDERS));
    }

    @Test
    public void shouldReturnAllCoursesTakenByAlice() throws Exception {
        String query = "students has first = Alice";

        List<Course> courses = courseRepository.findAll(getSpecification(getFilter(query)));

        assertThat(courses, hasSize(2));
        assertThat(courses, hasItems(LADDERS, STUDIOLOGY));
    }

    @Test
    public void shouldReturnAllCoursesTakenByAliceWithStream() throws Exception {
        String query = "students has first = Alice";

        Filter<Course> filter = getFilter(query);

        List<Course> courses = courseRepository.findAll()
                .stream()
                .filter(filter)
                .collect(toList());

        assertThat(courses, hasSize(2));
        assertThat(courses, hasItems(LADDERS, STUDIOLOGY));
    }

    @Test
    public void shouldReturnAllStudentsEnrolledInACourseTaughtByProfessorsonAndWhomAreBornAfter1990() throws Exception {
        String query = "courses has professor.last = Professorson " +
                "and courses has professor.first = Professor " +
                "and birthdate > 1990-01-01";

        List<Student> courses = studentRepository.findAll(getSpecification(getFilter(query)));

        assertThat(courses, hasSize(1));
        assertThat(courses, hasItem(MICHAEL));
    }

    @Test
    public void shouldReturnOfAllCoursesTaughtByAlicesProfessors() throws Exception {
        String query = "professor.courses has courseName = Ladders";

        List<Course> courses = courseRepository.findAll(getSpecification(getFilter(query)));

        assertThat(courses, hasSize(1));
        assertThat(courses, hasItems(LADDERS));
    }

    private <T> Specification<T> getSpecification(Filter<T> filter) {
        return filter.accept(new SearchQuery<>());
    }

    private <T> Filter<T> getFilter(String query) {
        return new QueryParser(new TokenizedQuery(query, new RestQLTokenFactory())).parse().accept(new RestQLFilterVisitor<>());
    }
}
