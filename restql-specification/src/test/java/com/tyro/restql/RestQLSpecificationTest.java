/*
 * Copyright (c) 2003 - 2017 Tyro Payments Limited.
 * Lv1, 155 Clarence St, Sydney NSW 2000.
 * All rights reserved.
 */
package com.tyro.restql;

import com.tyro.restql.filter.EqualsFilter;
import com.tyro.restql.filter.LessThanFilter;
import com.tyro.restql.filter.LikeFilter;
import com.tyro.restql.repo.StudentRepository;
import com.tyro.restql.repo.model.Student;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.List;

import static com.tyro.restql.EntityFactory.*;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;

@Transactional
@SpringBootTest
@RunWith(SpringRunner.class)
public class RestQLSpecificationTest {

    @Autowired
    private StudentRepository studentRepository;

    @Before
    public void setupData() throws Exception {
        studentRepository.save(alice());
        studentRepository.save(michael());
        studentRepository.save(alexander());
    }

    @Test
    public void shouldReturnStudentWithFirstNameAlice() throws Exception {
        List<Student> students = studentRepository.findAll(new SearchQuery<Student>().visit(new EqualsFilter<>("first", "Alice")));

        assertThat(students, hasSize(1));
        assertThat(students, hasItem(alice()));
    }

    @Test
    public void shouldReturnStudentWithLastNameNull() throws Exception {
        List<Student> students = studentRepository.findAll(new SearchQuery<Student>().visit(new EqualsFilter<>("last", null)));

        assertThat(students, hasSize(1));
        assertThat(students, hasItem(alexander()));
    }

    @Test
    public void shouldReturnStudentsInYearEight() throws Exception {
        List<Student> students = studentRepository.findAll(new SearchQuery<Student>().visit(new EqualsFilter<>("year", "8")));

        assertThat(students, hasSize(1));
        assertThat(students, hasItem(alice()));
    }

    @Test
    public void shouldReturnStudentsInYearEightFromStringValue() throws Exception {
        List<Student> students = studentRepository.findAll(new SearchQuery<Student>().visit(new EqualsFilter<>("year", "8")));

        assertThat(students, hasSize(1));
        assertThat(students, hasItem(alice()));
    }

    @Test
    public void shouldReturnStudentsBornOn1989August14() throws Exception {
        List<Student> students = studentRepository.findAll(new SearchQuery<Student>().visit(new EqualsFilter<>("birthdate", "1989-08-14")));

        assertThat(students, hasSize(1));
        assertThat(students, hasItem(alice()));
    }

    @Test
    public void shouldReturnStudentsWithFirstNameBeginingWithAl() throws Exception {
        List<Student> students = studentRepository.findAll(new SearchQuery<Student>().visit(new LikeFilter<>("first", "Al")));

        assertThat(students, hasSize(2));
        assertThat(students, hasItems(alice(), alexander()));
    }

    @Test
    public void shouldReturnStudentsWithBirthdaysBefore1990() throws Exception {
        List<Student> students = studentRepository.findAll(new SearchQuery<Student>().visit(new LessThanFilter<>("birthdate", "1990-01-01")));

        assertThat(students, hasSize(2));
        assertThat(students, hasItems(alice(), alexander()));
    }
}
