/*
 * Copyright (c) 2003 - 2017 Tyro Payments Limited.
 * Lv1, 155 Clarence St, Sydney NSW 2000.
 * All rights reserved.
 */
package com.tyro.restql.repo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static java.util.stream.Collectors.toList;

@Entity
public class Student extends Person {

    @Column
    private String studentId;

    @Column
    private Integer year;

    @ManyToMany(mappedBy = "students")
    private Set<Course> courses;

    Student() {
        super();
    }

    public Student(String first, String last, Integer year, LocalDate birthdate, String studentId, Set<Course> courses) {
        super(first, last, birthdate);
        this.year = year;
        this.studentId = studentId;
        this.courses = courses;
        this.courses.forEach(course -> course.enroll(this));
    }

    public Student(String first, String last, Integer year, LocalDate birthdate, String studentId) {
        this(first, last, year, birthdate, studentId, new HashSet<>());
    }

    public String getStudentId() {
        return studentId;
    }

    public Integer getYear() {
        return year;
    }

    public Set<Course> getCourses() {
        return courses;
    }

    public void addCourse(Course course) {
        this.courses.add(course);
    }

    public void removeCourse(Course course) {
        this.courses.remove(course);
    }

    @Override
    public String toString() {
        return "Student{" +
                "first='" + this.getFirst() + '\'' +
                ", last='" + this.getLast() + '\'' +
                ", birthdate='" + this.getBirthdate() + '\'' +
                ", studentId='" + studentId + '\'' +
                ", year=" + year + '\'' +
                ", courses=" + courses.stream().map(Course::getCourseName).collect(toList()) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(studentId, student.studentId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(studentId);
    }
}
