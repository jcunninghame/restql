package com.tyro.restql.repo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static java.util.stream.Collectors.toList;
import static javax.persistence.CascadeType.ALL;

@Entity
public class Professor extends Person {

    @Column
    private String facultyId;

    @OneToMany(mappedBy = "professor", cascade = ALL)
    private Set<Course> courses;

    Professor() {
        super();
    }

    public Professor(String first,
                     String last,
                     LocalDate birthdate,
                     String facultyId,
                     Set<Course> courses) {
        super(first, last, birthdate);
        this.facultyId = facultyId;
        this.courses = courses;
    }

    public Professor(String first,
                     String last,
                     LocalDate birthdate,
                     String facultyId) {
        this(first, last, birthdate, facultyId, new HashSet<>());
    }

    public String getFacultyId() {
        return facultyId;
    }

    public Set<Course> getCourses() {
        return courses;
    }

    public void addCourse(Course course) {
        this.courses.add(course);
    }

    @Override
    public String toString() {
        return "Professor{" +
                "first='" + this.getFirst() + '\'' +
                ", last='" + this.getLast() + '\'' +
                ", birthdate='" + this.getBirthdate() + '\'' +
                ", facultyId='" + facultyId + '\'' +
                ", courses=" + courses.stream().map(Course::getCourseName).collect(toList()) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Professor professor = (Professor) o;
        return Objects.equals(facultyId, professor.facultyId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(facultyId);
    }
}
