package com.tyro.restql.repo.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static java.util.stream.Collectors.toList;
import static javax.persistence.CascadeType.ALL;

@Entity
public class Course {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String courseName;

    @ManyToOne
    @JoinColumn(name = "PROFESSOR_ID")
    private Professor professor;

    @ManyToMany(cascade = ALL)
    @JoinTable(name = "STUDENT_COURSE",
            joinColumns = @JoinColumn(name = "COURSE_ID"),
            inverseJoinColumns = @JoinColumn(name = "STUDENT_ID"))
    private Set<Student> students;

    Course() {

    }

    public Course(String courseName, Professor professor, Set<Student> students) {
        this.courseName = courseName;
        this.professor = professor;
        this.professor.addCourse(this);
        this.students = students;
        this.students.forEach(student -> student.addCourse(this));
    }

    public Course(String courseName, Professor professor) {
        this(courseName, professor, new HashSet<>());
    }

    public String getCourseName() {
        return courseName;
    }

    public Professor getProfessor() {
        return professor;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public void enroll(Student alice) {
        this.students.add(alice);
        alice.addCourse(this);
    }

    @Override
    public String toString() {
        return "Course{" +
                "courseName='" + courseName + '\'' +
                ", professor=" + professor.getFirst() + " " + professor.getLast() + '\'' +
                ", students=" + students.stream().map(s -> s.getFirst() + " " + s.getLast()).collect(toList()) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course course = (Course) o;
        return Objects.equals(courseName, course.courseName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(courseName);
    }
}
