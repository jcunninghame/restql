package com.tyro.restql;

import com.tyro.restql.repo.model.Course;
import com.tyro.restql.repo.model.Professor;
import com.tyro.restql.repo.model.Student;

import static java.time.LocalDate.of;

public final class EntityFactory {

    public static Student alice() {
        return new Student("Alice", "Nicholson", 8, of(1989, 8, 14), "U_4518510");
    }

    public static Student michael() {
        return new Student("Michael", "Byzantine", 9, of(1991, 1, 1), "U_4557201");
    }

    public static Student alexander() {
        return new Student("Alexander", null, 12, of(1985, 4, 23), "U_459961");
    }

    public static Professor bradford() {
        return new Professor("Brad", "Bradford", of(1968, 3, 25), "F_3240583");
    }

    public static Professor professorson() {
        return new Professor("Professor", "Professorson", of(1974, 7, 2), "F_6843197");
    }

    public static Course ladders(Professor professor) {
        return new Course("Ladders", professor);
    }

    public static Course studiology(Professor professor) {
        return new Course("Studiology", professor);
    }
}
