package com.tyro.restql.data;

import java.io.Serializable;

public class CountryLanguageKey implements Serializable {
    private String countryCode;
    private String language;
}
