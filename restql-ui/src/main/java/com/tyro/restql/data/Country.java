package com.tyro.restql.data;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;

@Entity
public class Country {

    @Id
    private String code;

    @Column
    private String name;

    @Column
    private String continent;

    @Column
    private String region;

    @Column
    private Double surfaceArea;

    @Column
    private Integer indepYear;

    @Column
    private Integer population;

    @Column
    private Double lifeExpectancy;

    @Column
    private Double gNP;

    @Column
    private Double gNPOld;

    @Column
    private String localName;

    @Column
    private String governmentForm;

    @Column
    private String headOfState;

    @Column
    private String capital;

    @Column
    private String code2;

    @OneToMany(mappedBy = "country", cascade = ALL)
    private Set<City> cities;

    public Country() {
    }

    public Country(String code,
                   String name,
                   String continent,
                   String region,
                   double surfaceArea,
                   int indepYear,
                   int population,
                   double lifeExpectancy,
                   double gNP,
                   double gNPOld,
                   String localName,
                   String governmentForm,
                   String headOfState,
                   String capital,
                   String code2,
                   Set<City> cities) {
        this.code = code;
        this.name = name;
        this.continent = continent;
        this.region = region;
        this.surfaceArea = surfaceArea;
        this.indepYear = indepYear;
        this.population = population;
        this.lifeExpectancy = lifeExpectancy;
        this.gNP = gNP;
        this.gNPOld = gNPOld;
        this.localName = localName;
        this.governmentForm = governmentForm;
        this.headOfState = headOfState;
        this.capital = capital;
        this.code2 = code2;
        this.cities = cities;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getContinent() {
        return continent;
    }

    public String getRegion() {
        return region;
    }

    public Double getSurfaceArea() {
        return surfaceArea;
    }

    public Integer getIndepYear() {
        return indepYear;
    }

    public Integer getPopulation() {
        return population;
    }

    public Double getLifeExpectancy() {
        return lifeExpectancy;
    }

    public Double getgNP() {
        return gNP;
    }

    public Double getgNPOld() {
        return gNPOld;
    }

    public String getLocalName() {
        return localName;
    }

    public String getGovernmentForm() {
        return governmentForm;
    }

    public String getHeadOfState() {
        return headOfState;
    }

    public String getCapital() {
        return capital;
    }

    public String getCode2() {
        return code2;
    }

    @Override
    public String toString() {
        return "Country{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", continent='" + continent + '\'' +
                ", region='" + region + '\'' +
                ", surfaceArea=" + surfaceArea +
                ", indepYear=" + indepYear +
                ", population=" + population +
                ", lifeExpectancy=" + lifeExpectancy +
                ", gNP=" + gNP +
                ", gNPOld=" + gNPOld +
                ", localName='" + localName + '\'' +
                ", governmentForm='" + governmentForm + '\'' +
                ", headOfState='" + headOfState + '\'' +
                ", capital='" + capital + '\'' +
                ", code2='" + code2 + '\'' +
                ", cities=" + cities +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other, "id", "cities");
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this, "id", "cities");
    }
}
