package com.tyro.restql.model;

import com.tyro.restql.grammar.token.*;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class GrammarToken {

    private String tokenType;
    private String symbol;
    private int precedence;
    private int rightPrecedence;
    private int nextPrecedence;

    public GrammarToken() {
    }

    public GrammarToken(String tokenType, String symbol, int precedence, int rightPrecedence, int nextPrecedence) {
        this.tokenType = tokenType;
        this.symbol = symbol;
        this.precedence = precedence;
        this.rightPrecedence = rightPrecedence;
        this.nextPrecedence = nextPrecedence;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public int getPrecedence() {
        return precedence;
    }

    public void setPrecedence(int precedence) {
        this.precedence = precedence;
    }

    public int getRightPrecedence() {
        return rightPrecedence;
    }

    public void setRightPrecedence(int rightPrecedence) {
        this.rightPrecedence = rightPrecedence;
    }

    public int getNextPrecedence() {
        return nextPrecedence;
    }

    public void setNextPrecedence(int nextPrecedence) {
        this.nextPrecedence = nextPrecedence;
    }

    public Token toToken() {
        if (tokenType.equals(Binary.class.getSimpleName())) {
            return new Binary(symbol, precedence, rightPrecedence, nextPrecedence);
        } else if (tokenType.equals(Prefix.class.getSimpleName())) {
            return new Prefix(symbol, precedence, rightPrecedence, nextPrecedence);
        } else if (tokenType.equals(Postfix.class.getSimpleName())) {
            return new Postfix(symbol, precedence, rightPrecedence, nextPrecedence);
        } else if (tokenType.equals(GroupOpen.class.getSimpleName())) {
            return new GroupOpen(symbol, precedence, rightPrecedence, nextPrecedence);
        } else if (tokenType.equals(GroupClose.class.getSimpleName())) {
            return new GroupClose(symbol, precedence, rightPrecedence, nextPrecedence);
        } else{
            throw new NotImplementedException("TODO");
        }
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}
