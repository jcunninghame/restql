package com.tyro.restql.model;

import com.tyro.restql.grammar.tree.BinaryNode;
import com.tyro.restql.grammar.tree.UnaryNode;
import com.tyro.restql.grammar.tree.ValueNode;
import com.tyro.restql.grammar.visitor.QueryVisitor;

public class QueryTreeBuilder implements QueryVisitor<QueryTreeNode> {

    @Override
    public QueryTreeNode visit(BinaryNode binaryNode) {
        return new QueryTreeNode(binaryNode.getToken().getSymbol(),
                binaryNode.getLeftChild().accept(this),
                binaryNode.getRightChild().accept(this));
    }

    @Override
    public QueryTreeNode visit(UnaryNode unaryNode) {
        return new QueryTreeNode(unaryNode.getToken().getSymbol(),
                unaryNode.getChild().accept(this));
    }

    @Override
    public QueryTreeNode visit(ValueNode valueNode) {
        return new QueryTreeNode(valueNode.getToken().getSymbol());
    }
}
