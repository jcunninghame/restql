package com.tyro.restql.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;

import static java.util.Arrays.asList;

public class QueryTreeNode {

    private final String symbol;
    private final List<QueryTreeNode> children;

    public QueryTreeNode(String symbol, QueryTreeNode... children) {
        this.symbol = symbol;
        this.children = asList(children);
    }

    public String getSymbol() {
        return symbol;
    }

    public List<QueryTreeNode> getChildren() {
        return children;
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
