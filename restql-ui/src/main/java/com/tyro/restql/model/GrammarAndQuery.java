package com.tyro.restql.model;

import com.tyro.restql.grammar.token.Token;

import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import static java.util.stream.Collectors.toMap;
import static org.codehaus.groovy.runtime.InvokerHelper.asList;

public class GrammarAndQuery {

    private String query;
    private List<GrammarToken> tokens;

    public GrammarAndQuery() {
    }

    public GrammarAndQuery(GrammarToken... tokens) {
        this(asList(tokens));
    }

    public GrammarAndQuery(List<GrammarToken> tokens) {
        this.query = "";
        this.tokens = tokens;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public List<GrammarToken> getTokens() {
        return tokens;
    }

    public void setTokens(List<GrammarToken> tokens) {
        this.tokens = tokens;
    }

    public void addToken(GrammarToken terminalToken) {
        this.tokens.add(terminalToken);
    }

    public void removeToken(int i) {
        tokens.remove(i);
    }

    public Map<String, Supplier<Token>> getTokenFactory() {
        return tokens.stream().collect(toMap(GrammarToken::getSymbol, token -> (token::toToken)));
    }
}
