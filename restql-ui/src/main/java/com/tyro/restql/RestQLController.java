package com.tyro.restql;

import com.tyro.restql.data.Country;
import com.tyro.restql.filter.Filter;
import com.tyro.restql.filter.factory.RestQLTokenFactory;
import com.tyro.restql.filter.visitor.RestQLFilterVisitor;
import com.tyro.restql.grammar.QueryParser;
import com.tyro.restql.grammar.TokenizedQuery;
import com.tyro.restql.grammar.exception.GrammarException;
import com.tyro.restql.grammar.tree.Node;
import com.tyro.restql.model.QueryCommand;
import com.tyro.restql.repo.CityRepository;
import com.tyro.restql.repo.CountryLanguageRepository;
import com.tyro.restql.repo.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class RestQLController {

    public final CityRepository cityRepository;
    public final CountryRepository countryRepository;
    public final CountryLanguageRepository countryLanguageRepository;

    @Autowired
    public RestQLController(CityRepository cityRepository, CountryRepository countryRepository, CountryLanguageRepository countryLanguageRepository) {
        this.cityRepository = cityRepository;
        this.countryRepository = countryRepository;
        this.countryLanguageRepository = countryLanguageRepository;
    }

    @GetMapping("/restql")
    public String restqlForm(Model model) {
        model.addAttribute("queryCommand", new QueryCommand());
        return "restql";
    }

    @PostMapping("/restql")
    public String queryCountryRepository(@ModelAttribute QueryCommand queryCommand, Model model) {
        model.addAttribute("queryCommand", queryCommand);

        try {
            TokenizedQuery tokenizedQuery = new TokenizedQuery(queryCommand.getQuery(), new RestQLTokenFactory());
            Node queryTree = new QueryParser(tokenizedQuery).parse();
            Filter<Country> filter = queryTree.accept(new RestQLFilterVisitor<>());
            Specification<Country> specification = filter.accept(new SearchQuery<>());
            List<Country> countries = countryRepository.findAll(specification);
            model.addAttribute("result", "Found " + countries.size() + " query results");
            model.addAttribute("countries", countries);
        } catch (GrammarException e) {
            String result = e.getLocalizedMessage();
            model.addAttribute("result", result);
        }

        return "restql";
    }
}
