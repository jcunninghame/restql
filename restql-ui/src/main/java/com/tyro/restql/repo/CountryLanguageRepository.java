package com.tyro.restql.repo;

import com.tyro.restql.data.CountryLanguage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CountryLanguageRepository extends JpaRepository<CountryLanguage, Long>, JpaSpecificationExecutor<CountryLanguage> {
}
