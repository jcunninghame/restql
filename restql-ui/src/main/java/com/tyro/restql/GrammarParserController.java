package com.tyro.restql;

import com.google.gson.Gson;
import com.tyro.restql.filter.factory.RestQLTokenFactory;
import com.tyro.restql.grammar.QueryParser;
import com.tyro.restql.grammar.TokenizedQuery;
import com.tyro.restql.grammar.exception.GrammarException;
import com.tyro.restql.grammar.token.*;
import com.tyro.restql.grammar.tree.Node;
import com.tyro.restql.model.GrammarAndQuery;
import com.tyro.restql.model.GrammarToken;
import com.tyro.restql.model.QueryTreeBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

@Controller
public class GrammarParserController {

    private static final String TERMINAL = Terminal.class.getSimpleName();
    private static final String BINARY = Binary.class.getSimpleName();
    private static final String PREFIX = Prefix.class.getSimpleName();
    private static final String POSTFIX = Postfix.class.getSimpleName();
    private static final String OPEN = GroupOpen.class.getSimpleName();
    private static final String CLOSE = GroupClose.class.getSimpleName();

    @GetMapping("/parse-grammar")
    public String initRecursiveDecentParser() {
        return "grammar";
    }

    @PostMapping("/parse-grammar")
    public String parseQueryOverTokens(@ModelAttribute GrammarAndQuery grammarAndQuery, Model model) {
        model.addAttribute("grammarAndQuery", grammarAndQuery);

        try {
            Node queryTree = new QueryParser(new TokenizedQuery(grammarAndQuery.getQuery(), grammarAndQuery.getTokenFactory())).parse();
            model.addAttribute("result", queryTree);
            model.addAttribute("queryTree", new Gson().toJson(queryTree.accept(new QueryTreeBuilder())));
        } catch (GrammarException e) {
            model.addAttribute("result", e.getLocalizedMessage());
            model.addAttribute("queryTree", "{}");
        }

        return "grammar";
    }

    @PostMapping(value = "/parse-grammar", params = {"addToken"})
    public String addToken(@ModelAttribute GrammarAndQuery grammarAndQuery) {
        GrammarToken terminalToken = new GrammarToken(Terminal.class.getSimpleName(), ".", -1, -1, -1);
        grammarAndQuery.addToken(terminalToken);
        return "grammar";
    }

    @PostMapping(value = "/parse-grammar", params = {"removeToken"})
    public String removeToken(@ModelAttribute GrammarAndQuery grammarAndQuery, final HttpServletRequest req) {
        grammarAndQuery.removeToken(Integer.valueOf(req.getParameter("removeToken")));
        return "grammar";
    }

    @ModelAttribute("tokenTypes")
    public List<String> populateTokenTypes() {
        return Arrays.asList(TERMINAL, BINARY, PREFIX, POSTFIX, OPEN, CLOSE);
    }

    @ModelAttribute("grammarAndQuery")
    public GrammarAndQuery recursiveDecentParser() {
        return new GrammarAndQuery(
                new RestQLTokenFactory()
                        .values()
                        .stream()
                        .map(Supplier::get)
                        .map(token -> new GrammarToken(
                                token.getClass().getSimpleName(),
                                token.getSymbol(),
                                token.precedence(),
                                token.rightPrecedence(),
                                token.nextPrecedence()))
                        .toArray(GrammarToken[]::new)
        );
    }
}
